<div class="content-logo">
    <div class="logo">
        <span>API Documentation</span>
    </div>
</div>
<div class="mobile-menu-closer"></div>
<div class="content-menu">
    <div class="content-infos">
        <div class="info">
            <b>Version:</b>
             <select name="version" class="form-control">
                <option value="" selected>-</option>
                @foreach(app(\RubenEngels\ApiDocGen\Services\RouteConverterService::class)->listVersions() as $version)
                    <option {{ ($selectedVersion === $version) ? 'selected' : '' }} value="{{ $version }}">{{ $version }}</option>
                @endforeach
             </select>
         </div>
    </div>
    <ul>
        @forelse($endpoints ?? [] as $endpoint)
            <li class="scroll-to-link" endpoint="{{ $selectedVersion . '/' . $endpoint }}">
                <a>{{ $endpoint }}</a>
            </li>
        @empty
            <li class="scroll-to-link active">
                <a>Select a version to see it's endpoints</a>
            </li>
        @endforelse
    </ul>
</div>
