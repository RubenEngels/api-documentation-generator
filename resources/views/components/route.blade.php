<div route-item>
    <h1>{{ $route->uri }}</h1>

    <span>Parameters:</span>
    @forelse($route->getParameters() as $param)
        {{ $param }}
    @empty
        <p>This route has no parameters</p>
    @endforelse
</div>
