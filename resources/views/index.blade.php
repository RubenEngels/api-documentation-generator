@extends('ApiDocGen::base')

@section('content')
    
    <div class="content-page">
        <div class="content-code"></div>
        <div class="content">
            <div class="overflow-hidden content-section" id="content-get-started">
                <h1>Get started</h1>
                <pre>
        API Endpoint

            https://api.westeros.com/
                    </pre>
                <p>
                    The Westeros API provides programmatic access to read Game of Thrones data. Retrieve a character, provide an oauth connexion, retrieve a familly, filter them, etc.
                </p>
                <p>
                    To use this API, you need an <strong>API key</strong>. Please contact us at <a href="mailto:jon.snow@nightswatch.wes">jon.snow@nightswatch.wes</a> to get your own API key.
                </p>
            </div>
            <div class="overflow-hidden content-section" id="content-get-characters">
                <h2>get characters</h2>
                <pre><code class="bash">
    # Here is a curl example
    curl \
    -X POST http://api.westeros.com/character/get \
    -F 'secret_key=your_api_key' \
    -F 'house=Stark,Bolton' \
    -F 'offset=0' \
    -F 'limit=50'
                    </code></pre>
                <p>
                    To get characters you need to make a POST call to the following url :<br>
                    <code class="higlighted break-word">http://api.westeros.com/character/get</code>
                </p>
                <br>
                <pre><code class="json">
    Result example :

    {
      query:{
        offset: 0,
        limit: 50,
        house: [
          "Stark",
          "Bolton"
        ],
      }
      result: [
        {
          id: 1,
          first_name: "Jon",
          last_name: "Snow",
          alive: true,
          house: "Stark",
          gender: "m",
          age: 14,
          location: "Winterfell"
        },
        {
          id: 2,
          first_name: "Eddard",
          last_name: "Stark",
          alive: false,
          house: "Stark",
          gender: "m",
          age: 35,
          location: 'Winterfell'
        },
        {
          id: 3,
          first_name: "Catelyn",
          last_name: "Stark",
          alive: false,
          house: "Stark",
          gender: "f",
          age: 33,
          location: "Winterfell"
        },
        {
          id: 4,
          first_name: "Roose",
          last_name: "Bolton",
          alive: false,
          house: "Bolton",
          gender: "m",
          age: 40,
          location: "Dreadfort"
        },
        {
          id: 5,
          first_name: "Ramsay",
          last_name: "Snow",
          alive: false,
          house: "Bolton",
          gender: "m",
          age: 15,
          location: "Dreadfort"
        },
      ]
    }
                    </code></pre>
                <h4>QUERY PARAMETERS</h4>
                <table class="central-overflow-x">
                    <thead>
                    <tr>
                        <th>Field</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>secret_key</td>
                        <td>String</td>
                        <td>Your API key.</td>
                    </tr>
                    <tr>
                        <td>search</td>
                        <td>String</td>
                        <td>(optional) A search word to find character by name.</td>
                    </tr>
                    <tr>
                        <td>house</td>
                        <td>String</td>
                        <td>
                            (optional) a string array of houses:
                        </td>
                    </tr>
                    <tr>
                        <td>alive</td>
                        <td>Boolean</td>
                        <td>
                            (optional) a boolean to filter alived characters
                        </td>
                    </tr>
                    <tr>
                        <td>gender</td>
                        <td>String</td>
                        <td>
                            (optional) a string to filter character by gender:<br>
                            m: male<br>
                            f: female
                        </td>
                    </tr>
                    <tr>
                        <td>offset</td>
                        <td>Integer</td>
                        <td>(optional - default: 0) A cursor for use in pagination. Pagination starts offset the specified offset.</td>
                    </tr>
                    <tr>
                        <td>limit</td>
                        <td>Integer</td>
                        <td>(optional - default: 10) A limit on the number of objects to be returned, between 1 and 100.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="overflow-hidden content-section" id="content-errors">
                <h2>Errors</h2>
                <p>
                    The Westeros API uses the following error codes:
                </p>
                <table>
                    <thead>
                    <tr>
                        <th>Error Code</th>
                        <th>Meaning</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>X000</td>
                        <td>
                            Some parameters are missing. This error appears when you don't pass every mandatory parameters.
                        </td>
                    </tr>
                    <tr>
                        <td>X001</td>
                        <td>
                            Unknown or unvalid <code class="higlighted">secret_key</code>. This error appears if you use an unknow API key or if your API key expired.
                        </td>
                    </tr>
                    <tr>
                        <td>X002</td>
                        <td>
                            Unvalid <code class="higlighted">secret_key</code> for this domain. This error appears if you use an  API key non specified for your domain. Developper or Universal API keys doesn't have domain checker.
                        </td>
                    </tr>
                    <tr>
                        <td>X003</td>
                        <td>
                            Unknown or unvalid user <code class="higlighted">token</code>. This error appears if you use an unknow user <code class="higlighted">token</code> or if the user <code class="higlighted">token</code> expired.
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="content-code"></div>
    </div>
@endsection
