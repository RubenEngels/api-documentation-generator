<?php

namespace RubenEngels\ApiDocGen\Services;

use Illuminate\Routing\Route;

class RouteAnalyzerService {

    const DOC_PATTERN = '#(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)#';
    private $route;

    public function __construct(Route $route) {
        $this->route = $route;

        $this->setActionDetails();
    }

    private function setActionDetails() {
        $action = $this->route->action['uses'];

        $class = explode('@', $action)[0];
        $method = explode('@', $action)[1];

        $this->classReflection = new \ReflectionClass($class);
        $this->methodReflection = $this->classReflection->getMethod($method);
    }

    public function getActionDetails(): array {
        return [
             'action' => $tihs->route->action['uses'],
             'class' => $this->classReflection,
             'method' => $this->methodReflection
        ];
    }

    public function getRoute(): Route {
        return $this->route;
    }

    public function getMethodParameters(): array {
        return collect($this->methodReflection->getParameters());
    }

    public function getMethodReturnType():? ReflectionType {
        return $this->methodReflection->getReturnType();
    }

    public function getRouteUri(): string {
        return $this->route->uri;
    }
}
