<?php

namespace RubenEngels\ApiDocGen\Services;

use Illuminate\Support\Facades\Route as RouteList;
use Illuminate\Routing\Route;
use Illuminate\Support\Collection;
use RubenEngels\ApiDocGen\Exceptions\NoApiVersionException;

class RouteConverterService {
    private $routes;
    private $apiRoutes;

    public function __construct() {
        $routes = RouteList::getRoutes()->getRoutes();

        $this->routes = collect($routes); // collect so that we have the collection function available

        $this->apiRoutes = $this->routes->filter(function (Route $route) {
            return str_contains($route->uri, 'api/');
        })->values(); // use values() to reindex keys
    }

    // list only the versions for the api routes
    public function listVersions(): Collection {
        $this
            ->mapToVersions()
        ;

        return $this->apiRoutes->keys();
    }

    public function listEndpointsForVersion(string $version):? Collection {
        $this->mapApiRoutes();

        return ($this->apiRoutes->get($version)) ? $this->apiRoutes->get($version)->keys() : null;
    }

    public function listMethodsForEndpointAndVersion(string $version, string $endpoint):? Collection {
        $this->mapApiRoutes();

        if ($endpoints = $this->apiRoutes->get($version)) {
            return $endpoints->get($endpoint);
        }

        return null;
    }

    // sort the routes per api version for display purposes
    public function mapApiRoutes(): Collection {
        $this
          ->mapToVersions()
          ->mapToEndpoints()
        ;

        return $this->apiRoutes;
    }

    // map to each seperate api version, read: v1, v2, v3 etc...
    protected function mapToVersions(): RouteConverterService {
        $this->apiRoutes = $this->apiRoutes->mapToGroups(function (Route $route) {
            return [$this->getVersionFromApiRoute($route) => $route];
        });

        $this->apiRoutes = $this->apiRoutes->filter(fn ($item, string $version) => $version <> 'noversion');

        return $this;
    }

    //
    protected function mapToEndpoints(): RouteConverterService {
        $this->apiRoutes = $this->apiRoutes->map(function (Collection $versionGroup, string $version) {
            return $versionGroup->mapToGroups(function (Route $route) use ($version) {
                return [$this->getEndpointFromApiRoute($route, $version) => new RouteAnalyzerService($route)];
            });
        });

        return $this;
    }

    // by way of regex find the api endpoint (group) of the route
    private function getEndpointFromApiRoute(Route $route, string $version): string {
        $matches = [];

        preg_match('/api\/' . $version . '\/(?\'endpoint\'.*?)(?>\/|$)/', $route->uri . "\n", $matches); // add newline char because of not matching /api/v1/test for example

        if (!isset($matches['endpoint'])) {
            return '';
        }

        return $matches['endpoint'];
    }

    // by way of regex find the api version or throw a exception
    private function getVersionFromApiRoute(Route $route):? string {
        $matches = [];

        preg_match('/(?:api\/(?\'version\'v\d+)\/)/', $route->uri, $matches);

        if (!isset($matches['version'])) {
            if (config('apidocgen.strict_checking')) {
                throw new NoApiVersionException($route);
            }

            return 'noversion';
        }

        return $matches['version'];
    }

    public function getRoutes() {
      return $this->routes;
    }

    public function getApiRoutes() {
        return $this->apiRoutes;
    }
}
