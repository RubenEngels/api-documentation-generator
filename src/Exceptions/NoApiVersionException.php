<?php

namespace RubenEngels\ApiDocGen\Exceptions;

use Illuminate\Routing\Route;

class NoApiVersionException extends \Exception {

    private $route;

    // redefine constructor so that $message is not optional
    public function __construct(Route $route, $code = 0, Throwable $previous = null) {
        $this->route = $route;

        parent::__construct($this->composeMessage(), $code, $previous);
    }

    private function composeMessage(): string {
        return 'There has not been a api version found on the route: `' . $this->route->uri  . '`';
    }
}
