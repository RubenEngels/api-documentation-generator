<?php

namespace RubenEngels\ApiDocGen\Http\Controllers;

use Illuminate\Http\Request;
use RubenEngels\ApiDocGen\Services\RouteConverterService;

class DocumentationController extends Controller
{

    public function __construct() {
        \Artisan::call('vendor:publish', [
            '--force' => true,
            '--provider' => "RubenEngels\ApiDocGen\ApiDocGenServiceProvider",
            '--tag' => 'public'
        ]);
    }

    public function index() {
        return view('ApiDocGen::index', [
            'selectedVersion' => 0
        ]);
    }

    public function versionHandler(string $version) {
        return view('ApiDocGen::version', [
            'selectedVersion' => $version,
            'endpoints' => app(RouteConverterService::class)->listEndpointsForVersion($version)
        ]);
    }

    public function endpointHandler(string $version, $endpoint = '') {
        $routes = app(RouteConverterService::class)->listMethodsForEndpointAndVersion($version, $endpoint);

        return $endpoint;
    }
}
