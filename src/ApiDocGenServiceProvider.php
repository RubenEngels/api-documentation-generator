<?php

namespace RubenEngels\ApiDocGen;

use Illuminate\Support\ServiceProvider;

class ApiDocGenServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
     public function boot()
     {
         $this->publishes([
             __DIR__.'/../config/apidocgen.php' => config_path('apidocgen.php'),
         ]);

         $this->mergeConfigFrom( __DIR__ . '/../config/apidocgen.php', 'apidocgen.php');

         $this->publishes([
            __DIR__ . '/../public' => public_path('vendor/apidocgen'),
        ], 'public');

         $this->loadViewsFrom(__DIR__.'/../resources/views', 'ApiDocGen');
         $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
     }
}
