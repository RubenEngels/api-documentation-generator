<?php

namespace RubenEngels\ApiDocGen\Tests\Unit;

use Illuminate\Support\Collection;
use RubenEngels\ApiDocGen\Services\RouteConverterService;
use RubenEngels\ApiDocGen\Tests\TestCase;

class RouteConverterServiceTest extends TestCase {
    public function test_list_versions(): void {
        $versions = app(RouteConverterService::class)->listVersions();

        $this->assertTrue(is_a($versions, Collection::class));
    }
}