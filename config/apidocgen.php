<?php

return [
    'strict_checking' => env('API_DOC_STRICT_CHECKING', true)
];
