class ApiDocGen {
    constructor() {
        $('select[name="version"]').on('change', (e) => window.location.href = '/documentation/' + $(e.currentTarget).val());

        $('li[endpoint]').on('click', this.loadEndpoint.bind(this));
    }

    loadEndpoint(e) {
        $('li').each((it, el) => {
            $(el).removeClass('active');
        })

        $(e.currentTarget).addClass('active');

        $.get('/documentation/' + $(e.currentTarget).attr('endpoint') + '/', response => {
            $('[endpoints-wrapper]').html(response);
        })
    }
} new ApiDocGen;
