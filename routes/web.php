<?php

use Illuminate\Support\Facades\Route;
use RubenEngels\ApiDocGen\Services\RouteConverterService;
use RubenEngels\ApiDocGen\Http\Controllers\DocumentationController;
use Illuminate\Support\Facades\Route as RouteList;

Route::group(['prefix' => 'documentation', 'as' => 'documentation.'], function() {

    Route::get('/test', function () {
        $endpoints = app(RouteConverterService::class)->mapApiRoutes();
        // $test = new RubenEngels\ApiDocGen\Services\RouteAnalyzerService($endpoints['v1']['user'][0]);

        dd($endpoints);
    });

    Route::get('/', [DocumentationController::class, 'index']);

    Route::get('/{version}/', [DocumentationController::class, 'versionHandler']);

    Route::get('/{version}/{endpoint}', [DocumentationController::Class, 'endpointHandler']);
});
